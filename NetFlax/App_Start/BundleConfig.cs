﻿using System.Web;
using System.Web.Optimization;

namespace NetFlax
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));


            bundles.Add(new ScriptBundle("~/js/MonJs").Include(
                      "~/js/jquery.magnific-popup.js",
                      "~/js/jquery.min.js",
                      "~/js/jquery.flexisel.js"));


            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/style.css",
                      "~/Content/bootstrap.css"));
        }
    }
}
